<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public $id;
	public $username;
	public $password;

	public function get_last_ten_entries()
	{
			$query = $this->db->get('users', 10);
			return $query->result();
	}

	public function insert_entry()
	{
			$this->id    	 = $_POST['id']; // please read the below note
			$this->username  = $_POST['username'];
			$this->password  = $_POST['password'];

			$this->db->insert('users', $this);
	}

	public function update_entry()
	{
		$this->id    	 = $_POST['id']; // please read the below note
		$this->username  = $_POST['username'];
		$this->password  = $_POST['password'];


			$this->db->update('users', $this, array('id' => $_POST['id']));
	}

	public function delete_entry()
	{
		$this->id    	 = $_POST['id']; // please read the below note
		$this->username  = $_POST['username'];
		$this->password  = $_POST['password'];


		$this->db->delete('users', array('id' => $_POST['id']));
	}

}

